//+------------------------------------------------------------------+
//|                                               StringEnhanced.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property strict

#include <Arrays\List.mqh>
#include <Strings\String.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Arrays\ArrayString.mqh>


class String : public CString 
{
protected:
   CArrayObj      m_obj_array;
   ushort         m_separator;   // default = NULL = ' '; (space)
   int            m_digits;      // default = 5
   
public:
                  String():m_separator(NULL),m_digits(5) {}
   template<typename T>
                  String(const T text):m_separator(NULL),m_digits(5){ m_string = string(text);}
   //--- operators
   template<typename T>       
   String*        operator =  (const T text)       { m_string = string(text); return &this;  }
   String*        operator =  (CString &text)      { m_string = text.Str();   return &this;  }
   String*        operator =  (CString *text)      { m_string = text.Str();   return &this;  }
   template<typename T> 
   string         operator +  (const T text)       { return m_string + string(text);         }
   string         operator +  (CString &text)      { return m_string + text.Str();           }
   string         operator +  (CString *text)      { return m_string + text.Str();           }
   template<typename T> 
   string         operator -  (const T text)       { String temp = m_string; temp.Remove(string(text)); return temp.Str(); }
   string         operator -  (CString &text)      { String temp = m_string; temp.Remove(text.Str());   return temp.Str(); }
   string         operator -  (CString *text)      { String temp = m_string; temp.Remove(text.Str());   return temp.Str(); }
    template<typename T> 
   String*        operator -= (const T text)       { Remove(string(text)); return &this;     }
   String*        operator -= (CString &text)      { Remove(text.Str());   return &this;     }
   String*        operator -= (CString *text)      { Remove(text.Str());   return &this;     }
   template<typename T> 
   String*        operator += (const T text)       { m_string += string(text);   return &this;            }
   String*        operator += (CString &text)      { m_string += text.Str();     return &this;            }
   String*        operator += (CString *text)      { m_string += text.Str();     return &this;            }
   template<typename T> 
   bool           operator != (const T text)       { return !(Compare(string(text)) == 0);   }
   bool           operator != (CString &text)      { return !(Compare(&text) == 0);          }
   bool           operator != (CString *text)      { return !(Compare(text) == 0);           }
   template<typename T> 
   bool           operator == (const T text)       { return Compare(string(text)) == 0;      }
   bool           operator == (CString &text)      { return Compare(&text) == 0;             }
   bool           operator == (CString *text)      { return Compare(text) == 0;              }
   template<typename T> 
   bool           operator >  (const T text)       { return Compare(string(text)) > 0;       }
   bool           operator >  (CString &text)      { return Compare(&text) > 0;              }
   bool           operator >  (CString *text)      { return Compare(text) > 0;               }
   template<typename T> 
   bool           operator <  (const T text)       { return Compare(string(text)) < 0;       }
   bool           operator <  (CString &text)      { return Compare(&text) < 0;              }
   bool           operator <  (CString *text)      { return Compare(text) < 0;               }
   template<typename T> 
   bool           operator <= (const T text)       { return Compare(string(text)) <= 0;      }
   bool           operator <= (CString &text)      { return Compare(&text) <= 0;             }
   bool           operator <= (CString *text)      { return Compare(text) <= 0;              }
   template<typename T> 
   bool           operator >= (const T text)       { return Compare(string(text)) >= 0;      }
   bool           operator >= (CString &text)      { return Compare(&text) >= 0;             }
   bool           operator >= (CString *text)      { return Compare(text) >= 0;              }
   //int            operator /  (uchar separator)    { return Split(separator);                }
   String*        operator [] (const int index) const;
   
   //--- override and fix bugs from std lib
   string         Left(const uint count) const;
   template<typename T>
   String*        Str(const T text)       { m_string = string(text); return &this;           }
   String*        Assign(const string str){ m_string = str; return &this;                    }
   template<typename T> 
   String*        Assign(const T str)     { CString::Assign(const string str);return &this;  }
   String*        Append(const string str, uchar separator = NULL);//{ m_string+=str; return &this;                     }
   template<typename T> 
   String*        Append(const T str, uchar separator = NULL)     { return this.Append((string)str,separator);                 }
   template<typename T> 
   uint           Insert(const uint pos,const T substring);
   template<typename T> 
   int            Compare(const T str)const{ return CString::Compare((string)str);           }
   template<typename T> 
   int            CompareNoCase(const T str)const{ return CString::CompareNoCase((string)str);}
   template<typename T> 
   string         Trim(const T targets)   { return CString::Trim((string)targets);           }
   template<typename T> 
   string         TrimLeft(const T targets){ return CString::TrimLeft((string)targets);      }
   template<typename T> 
   string         TrimRight(const T targets){ return CString::TrimRight((string)targets);    }
   //--- methods find
   template<typename T> 
   int            Find(const uint start,const T substring) const { return CString::Find(start,(string)substring);}
   template<typename T> 
   int            FindRev(const T substring) const { return CString::FindRev((string) substring);}
   template<typename T> 
   uint           Remove(const T substring);
   template<typename T,typename T2> 
   String*        Replace(const T substring,const T2 newstring);
   
   //--- methods for working with string arrays
   int            Total()                          { return m_obj_array.Total();             }
   //--- get/set Digits - default = 5
   int            Digits() const { return m_digits;}
   String*        Digits(const int digits) { m_digits = digits; return &this; }
   String*        Digits(const string symbol) { int d=(int)SymbolInfoInteger(symbol,SYMBOL_DIGITS); m_digits=d>0?d:m_digits;return &this;}
   int            Split(uchar separator);   
   int            Split(ushort separator, CArrayObj &array);
   int            Split(ushort separator, string &array[]);  
   String*        Sort(uchar separator = NULL);
   String*        SortReverse(uchar separator = NULL);
   int            Compare(const CObject *node,const int mode =0)const;
   template<typename T>
   String*        FormString(const T &array[],uchar separator = ' ');
   //String*        FormString(CArrayString *array);
   //String*        FormString(CArrayString &array);
   //String*        FormString(CArrayObj    *array);
   //String*        FormString(CArrayObj    &array);
   //String*        FormString(CList        *array);
   //String*        FormString(CList        &array);
   
   string         Substr(int pos,int length=0)     { return ::StringSubstr(m_string,pos,length);}
   
   String*        Print()                          { ::Print(m_string);return &this;            }
   String*        Comment()                        { ::Comment(m_string);return &this;          }
   String*        Alert()                          { ::Alert(m_string);return &this;            }
   bool           MessageBox()                     { return ::MessageBox(m_string,NULL,MB_OKCANCEL)==1;}
   bool           SendNotification()               { return ::SendNotification(m_string);       }
   bool           SendMail(string subject="MQL mail"){ return ::SendMail(subject,m_string);     }
   
   
   string         BaseCurrency()                   { return Substr(0,3);                        }
   string         CounterCurrency()                { return Substr(3,3);                        }
   ushort         GetChar(int pos)                 { return ::StringGetCharacter(m_string,pos); }
   
   color          ToColor() { return ::StringToColor(m_string); }
   double         ToDouble(){ return ::StringToDouble(m_string);}
   int            ToInt()   { return (int)::StringToInteger(m_string);}
   datetime       ToTime()  { return ::StringToTime(m_string);     }
   int            ToShortArray(ushort &array[],int start=0,int count=-1)
                  {
                     return ::StringToShortArray(m_string,array,start,count);
                  }
   int            ToCharArray(uchar &array[],int start=0,int count=-1,uint codepage=CP_ACP)
                  {
                     return ::StringToCharArray(m_string,array,start,count,codepage);
                  } 
protected:
   string         _FormatDateTime(datetime var1,string comments);
public:
   //--- methods for formatting strings
   template<typename T1>
   String*    Format(T1);
   
   template<typename T1,typename T2>
   String*    Format(T1 var1,T2 var2)
   {
      return Format(var1).Format(var2);
   }
   
   template<typename T1,typename T2,typename T3>
   String*     Format(T1 var1,T2 var2,T3 var3)
   {
      return Format(var1,var2).Format(var3);
   }
   
   template<typename T1,typename T2,typename T3,typename T4>
   String*  Format(T1 var1,T2 var2,T3 var3,T4 var4)
   {   
      return  Format(var1,var2,var3).Format(var4);
   }
   
   template<typename T1,typename T2,typename T3,typename T4,typename T5>
   String*     Format(T1 var1,T2 var2,T3 var3,T4 var4,T5 var5)
   {
      return  Format(var1,var2,var3,var4).Format(var5);
   }
   
   template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6>
   String*     Format(T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6)
   {
      return  Format(var1,var2,var3,var4,var5).Format(var6);
   }
   
   template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7>
   String*     Format(T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7)
   {
      return  Format(var1,var2,var3,var4,var5,var6).Format(var7);
   }
   
   template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7,typename T8>
   String*     Format(T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7,T8 var8)
   {
      return  Format(var1,var2,var3,var4,var5,var6,var7).Format(var8);
   }
   
   template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7,typename T8,typename T9>
   String*     Format(T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7,T8 var8,T9 var9)
   {
      return  Format(var1,var2,var3,var4,var5,var6,var7,var8).Format(var9);
   }
   
   template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7,typename T8,typename T9,typename T10>
   String*     Format(T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7,T8 var8,T9 var9,T10 var10)
   {
      return  Format(var1,var2,var3,var4,var5,var6,var7,var8,var9).Format(var10);
   }
}; 
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
template<typename T1>
String* String::Format(T1 var1)
{
   int l = Find(0,"{");
   int r = Find(0,"}")+1;
   string type = typename(var1);
   string comments = "";
   string res = string(var1);
   if(l >= 0 && r >= 0)
   {   
      int count = r-l-2;
      comments = count <= 0 ? "" : ::StringSubstr(m_string,l+1,count);
      if(type == "double")
      { 
         int digits = comments == "" ? -1 : (int)::StringToInteger(comments);
    /**/ //::Print("Debug Comment Format = ",comments," yield integer = ",digits);
         if(0 <= digits && digits <= 10)
         {
             res = ::DoubleToString((double)var1,digits);
         }
         else
         {
             res = ::DoubleToString((double)var1,m_digits);
         }
      }
      else 
      if(type == "char" || type == "uchar" || type == "ushort")
      {
         res = ::CharToString((uchar)var1);
      }
      else
      if(type == "datetime")
      {
         res = _FormatDateTime(datetime(var1),comments);
      }
      
      string left = ::StringSubstr(m_string,0,l);
      if(l ==0) 
         left = "";
      string right = ::StringSubstr(m_string,r,Len() - r);    
      m_string = left+right;     
      if(l == 0)
      {
         m_string = res+right;
      }
      else
         Insert(l,res);
   }
   return &this;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string String::_FormatDateTime(datetime var1,string comments)
{
   //DD,MM,YYYY HH,MI,SS
   MqlDateTime time;
   TimeToStruct(var1,time);
   string temp="", com = comments;
   StringToUpper(com);
   StringReplace(com,"YYYY",string(time.year));
   temp= time.mon<10 ? "0"+string(time.mon):string(time.mon);
   StringReplace(com,"MM",temp);
   temp= time.day<10 ? "0"+string(time.day):string(time.day);
   StringReplace(com,"DD",temp);
   temp= time.hour<10 ? "0"+string(time.hour):string(time.hour);
   StringReplace(com,"HH",temp);
   temp= time.min<10 ? "0"+string(time.min):string(time.min);
   StringReplace(com,"MI",temp);
   temp= time.sec<10 ? "0"+string(time.sec):string(time.sec);
   StringReplace(com,"SS",temp);
   
   return com;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

String* String::Append(const string str, uchar separator = NULL)
{
   string app_char = separator == NULL ? "" : ::CharToString(separator);
   m_string+= str+app_char;
   return &this;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
template<typename T>
String* String::FormString(const T &array[],uchar separator = ' ')
{
   int total = ::ArraySize(array);
   if(total <= 0)
      return &this;
   m_obj_array.Clear();
   m_string = "";
   for(int i=0;i<total;i++)
   {
      m_string+= string(array[i])+CharToString(separator);
   }
   Split(separator);
   return &this;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int String::Split(uchar separator = NULL)
{
   ushort sep = separator == NULL ? NULL : ushort(separator);
   if(sep == NULL)
   {
      if(m_separator == NULL)
         m_separator = ushort(' ');
   }
   else
      m_separator = sep;
      
   string parsed[];
   int total = ::StringSplit(m_string,m_separator,parsed);
 
   m_obj_array.Clear();
   for(int i=0;i<total;i++)
      if(parsed[i] != "")
         m_obj_array.Add(new String(parsed[i]));   

   return m_obj_array.Total();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int String::Split(ushort separator,CArrayObj &array)
{
   if(separator == NULL)
      if(m_separator == NULL)
         m_separator = ushort(' ');
   else
      m_separator = separator;
      
   string parsed[];
   int total = ::StringSplit(m_string,m_separator,parsed);
 
   m_obj_array.Clear();
   array.Clear();
   for(int i=0;i<total;i++)
   {
      m_obj_array.Add(new String(parsed[i]));   
      array.Add(new String(parsed[i]));
   }
   return m_obj_array.Total();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int String::Split(ushort separator,string &array[])
{
   if(separator == NULL)
      if(m_separator == NULL)
         m_separator = ushort(' ');
   else
      m_separator = separator;

   int total = ::StringSplit(m_string,m_separator,array);
 
   m_obj_array.Clear();
   for(int i=0;i<total;i++)
   {
      m_obj_array.Add(new String(array[i]));   
   }
   return m_obj_array.Total();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
String* String::operator[](const int index) const
{
   return (String*)m_obj_array.At(index);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

int String::Compare(const CObject *node,const int mode=0)const
{
   CString* other = (CString*)node;
   if(this.Compare(other) > 0)
      if(mode == 0)
         return 1;
      else 
         return -1;
   if(this.Compare(other) < 0)
      if(mode == 0)
         return -1;
      else 
         return 1;
   return 0;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
String* String::Sort(uchar separator = NULL)
{
   if(Split(separator) <= 0)
      return &this;
   m_obj_array.Sort();
   m_string = "";
   for(int i=0;i<m_obj_array.Total();i++)
      m_string+=((String*)m_obj_array.At(i)).Str()+(i==m_obj_array.Total()-1 ? "" : CharToString(separator));
   return &this;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
String* String::SortReverse(uchar separator = NULL)
{
   if(Split(separator) <= 0)
      return &this;
   m_obj_array.Sort(1);
   m_string = "";
   for(int i=0;i<m_obj_array.Total();i++)
      m_string+=((String*)m_obj_array.At(i)).Str()+(i==m_obj_array.Total()-1 ? "" : CharToString(separator));
   return &this;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
template<typename T> 
uint String::Remove(const T substring)
{
   string sub = string(substring);
   int    result=0,len,pos=-1;
   string tmp;
//---
   len=StringLen(sub);
   while((pos=StringFind(m_string,sub,pos))>=0)
     {
      tmp= pos == 0 ? "" : StringSubstr(m_string,0,pos);
      m_string=tmp+StringSubstr(m_string,pos+len);
      result++;
     }
//--- result
   return(result);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
template<typename T> 
uint String::Insert(const uint pos,const T substring)
  {
   string sub = string(substring);
   string tmp= pos == 0 ? "" : StringSubstr(m_string,0,pos);
//---
   tmp+=sub;
   m_string=tmp+ StringSubstr(m_string,pos);
//--- result
   return(StringLen(m_string));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
template<typename T1,typename T2> 
String* String::Replace(const T1 substring,const T2 newstring)
  {
   string sub = string(substring);
   string news= string(newstring);
   int    result=0,len,pos=-1;
   string tmp;
//---
   len=StringLen(sub);
   while((pos=StringFind(m_string,sub,pos))>=0)
     {
      tmp= pos == 0 ? news : StringSubstr(m_string,0,pos)+news;
      m_string=tmp+StringSubstr(m_string,pos+len);
      // to eliminate possible loops
      pos+=StringLen(news);
      result++;
     }
//--- result
   return &this;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string String::Left(const uint count) const
  {
   return(count == 0 ? "" : StringSubstr(m_string,0,count));
  }
 
 
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
 
template<typename T1>
string    format(const string text,T1 var1)
{
   String res = text;
   res.Format(var1);
   return res.Str();
}
template<typename T1,typename T2>
string    format(const string text,T1 var1,T2 var2)
{
   return format(format(text,var1),var2);
}

template<typename T1,typename T2,typename T3>
string    format(const string text,T1 var1,T2 var2,T3 var3)
{
   return format(format(text,var1,var2),var3);
}

template<typename T1,typename T2,typename T3,typename T4>
string  format(const string text,T1 var1,T2 var2,T3 var3,T4 var4)
{   
   return  format(format(text,var1,var2,var3),var4);
}

template<typename T1,typename T2,typename T3,typename T4,typename T5>
string     format(const string text,T1 var1,T2 var2,T3 var3,T4 var4,T5 var5)
{
   return  format(format(text,var1,var2,var3,var4),var5);
}

template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6>
string     format(const string text,T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6)
{
   return  format(format(text,var1,var2,var3,var4,var5),var6);
}

template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7>
string     format(const string text,T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7)
{
   return  format(format(text,var1,var2,var3,var4,var5,var6),var7);
}

template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7,typename T8>
string     format(const string text,T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7,T8 var8)
{
   return  format(format(text,var1,var2,var3,var4,var5,var6,var7),var8);
}

template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7,typename T8,typename T9>
string     format(const string text,T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7,T8 var8,T9 var9)
{
   return  format(format(text,var1,var2,var3,var4,var5,var6,var7,var8),var9);
}

template<typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7,typename T8,typename T9,typename T10>
string     format(const string text,T1 var1,T2 var2,T3 var3,T4 var4 ,T5 var5,T6 var6,T7 var7,T8 var8,T9 var9,T10 var10)
{
   return  format(format(text,var1,var2,var3,var4,var5,var6,var7,var8,var9),var10);
}
