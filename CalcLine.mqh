//+------------------------------------------------------------------+
//|                                                    CalcLines.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#include <ChartObjects\ChartObjectsLines.mqh>
#include <ChartObjects\ChartObjectsTxtControls.mqh>
#include <Controls\BmpButton.mqh>
#include "StringEnhanced.mqh"

enum LINE_TYPE
{
   LINE_OPEN,
   LINE_STOP,
   LINE_TAKE
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CalcLine : public CChartObjectHLine
{
private:
   LINE_TYPE         m_type;
   double            m_price;
   string            m_new_name;
   color             m_new_color;
   string            m_des;
   bool              m_hidden;
   
public:
                     CalcLine(LINE_TYPE type,double price);
                    ~CalcLine();   
   LINE_TYPE         LineType()  const { return m_type;}
   double            Price()     const { return (m_hidden ? 0 : CChartObjectHLine::Price(0));}
   bool              Hide();
   bool              Show();
   bool              ModDescription(string des);
protected:
   void              InitLine();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CalcLine::CalcLine(LINE_TYPE type,double price):m_type(type),
                                                m_price(price),
                                                m_hidden(false)
{
   InitLine();
   m_des=m_new_name;
   Create(0,m_new_name,0,price);
   Style(STYLE_DASHDOT);
   Color(m_new_color);
   Description(m_new_name);
   Selectable(true);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CalcLine::~CalcLine()
{
}
//+------------------------------------------------------------------+
void CalcLine::InitLine()
{
   switch(m_type)
   {
      case LINE_OPEN:
         m_new_color = clrYellow;
         m_new_name  = "Open_Price";
         break;
      case LINE_STOP:
         m_new_color = clrTomato;
         m_new_name  = "Stop_Loss";
         break;
      case LINE_TAKE:
         m_new_color = clrGreenYellow;
         m_new_name  = "Take_Profit";
         break;  
   }
}
//+------------------------------------------------------------------+
bool CalcLine::Hide(void)
{
   if(m_chart_id == -1)
      return true;
   m_price = Price();
   m_hidden = true;  
   return Delete();
}
//+------------------------------------------------------------------+
bool CalcLine::Show(void)
{
   bool res = Create(0,m_new_name,0,m_price);
   if(res)
   {
      m_hidden = false;
      Style(STYLE_DASHDOT);
      Color(m_new_color);
      Description(m_new_name);
      Selectable(true);
   }
   return res;
}

bool CalcLine::ModDescription(string des)
{
   m_des=des;
   return Description(des);
}