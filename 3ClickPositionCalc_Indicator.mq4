//+------------------------------------------------------------------+
//|                                                    Risk_Calc.mq4 |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                      Comment.mqh |
//|                                                        avoitenko |
//|                        https://login.mql5.com/en/users/avoitenko |
//+------------------------------------------------------------------+
#property copyright     "nicholishen"
#property link          "https://www.reddit.com/u/nicholishenFX"
#property version       "1.02"
#property icon          "calc_icon.ico"
#property description   "1. Click on the icon to activate the calculator."
#property description   "2. Once active, click on the chart in the places you want to set your order levels."
#property description   "3. Drag the levels to desired price points."
#property description   "4. Triple clicking the chart will clear the levels so you can start fresh."
#property indicator_chart_window
#property strict
#include "RiskCalc.mqh"
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
input ANCHOR   Location = RIGHT_UPPER; //Button Location
input double   Risk     = 1.0;         //Risk Percent
input double   Money    = 0.0;         //Money in reserves not on deposit @ broker.

RiskCalc calc;
int OnInit()
{
   calc.Init(Location,Risk,Money);
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
   calc.OnTimer();
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int    id,
                  const long   &lparam,
                  const double &dparam,
                  const string &sparam)
{
   calc.OnChartEvent(id,lparam,dparam,sparam);
}
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int      rates_total,
                const int      prev_calculated,
                const datetime &time[],
                const double   &open[],
                const double   &high[],
                const double   &low[],
                const double   &close[],
                const long     &tick_volume[],
                const long     &volume[],
                const int      &spread[])
{
   calc.Calc();
   return(rates_total);
}