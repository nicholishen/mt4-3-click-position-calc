//+------------------------------------------------------------------+
//|                                                     RiskCalc.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#include <stdlib.mqh>
#include "objvector.mqh"
#include "CalcLine.mqh"
#include "MyComment.mqh"
#resource "\\Images\\icon_calc_on.bmp"
#resource "\\Images\\icon_calc_off.bmp"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class RiskCalc : public objvector<CalcLine*>
{
protected:
   long              m_id;
   double            m_risk;
   MyComment         m_comment;
   string            m_comment_name;
   CBmpButton        m_button;
   string            m_button_name;
   CChartObjectLabel m_label;
   string            m_label_name;
   ANCHOR            m_anchor;
   XYXY              m_zone;
   XYXY              m_label_xy;
   int               m_clk_cnt;
   double            m_backup;
public:
                     RiskCalc();
                    ~RiskCalc();
   void              Init(ANCHOR,double,double);
   void              Calc();
   void              OnTimer();     
   void              OnChartEvent(int,long,double,string);
protected:
   bool              ModDescription(LINE_TYPE,string);
   bool              MouseClick(int,int);
   void              ClearMissing();
   bool              LineExists(LINE_TYPE type);
   void              ButtonMove();
   
   double            Open();
   double            Take();
   double            Stop();
   void              Show();
   void              Hide();  
   
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
RiskCalc::RiskCalc():m_id           (::ChartID()),
                     m_comment_name ("__Risk_Comment_"+string(m_id)),
                     m_button_name  ("__Risk_Button_"+string(m_id)),
                     m_label_name   ("__Risk_Label_"+string(m_id)),
                     m_clk_cnt      (0),
                     m_backup       (0)
{
   ::ObjectsDeleteAll(m_id,"__Risk");
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
RiskCalc::~RiskCalc()
{
}
//+------------------------------------------------------------------+
void RiskCalc::Init(ANCHOR anchor,double risk,double backup)
{
   m_backup = backup;
   m_risk   = risk / 100;
   m_anchor = anchor;
   if(!m_button.Create(0,m_button_name,0,10,10,100,100))
      Print(__FUNCTION__+" Error: Failed to create BmpButton");
   if(!m_button.BmpNames("::Images\\icon_calc_on.bmp","::Images\\icon_calc_off.bmp"))
      Print(__FUNCTION__+" Error: Setting BMP failed "+ErrorDescription(GetLastError()));
   m_button.Pressed(true);
   m_button.Text("TEST");
   ButtonMove();
   ChartSetInteger(0,CHART_SHOW_OBJECT_DESCR,true);
   ChartSetInteger(0,CHART_SHIFT,true);
   ObjectSetString(0,m_button_name,OBJPROP_TOOLTIP,"Toggle calc\nTriple-click chart to clear levels.");
   m_comment.Init(m_comment_name,m_anchor);
   m_comment.SetText(0,"This is a test of the comment box",COLOR_TEXT);
   //m_comment.Show();
}
//+------------------------------------------------------------------+
void RiskCalc::Calc(void)
{
   String s;
   int c=0;
   double div=1;
   double pVal   = SymbolInfoDouble(Symbol(),SYMBOL_TRADE_TICK_VALUE);
   if(_Digits == 5 || _Digits==3)
   {
      pVal*=10;
      div=10;
   }
   
   //m_comment.Clear();
   (s="Symbol: {} | Contract Size: {0}").Format(Symbol(),SymbolInfoDouble(Symbol(),SYMBOL_TRADE_CONTRACT_SIZE));
   m_comment.SetText(c++,s.Str(),COLOR_TEXT);
   (s="Pip Value: {2}").Format(pVal);
   m_comment.SetText(c++,s.Str(),COLOR_TEXT);
   double atr = iATR(Symbol(),PERIOD_D1,14,0);
   if(atr>0)
   {
      (s="Average Daily Range: {1} pips").Format(atr/_Point/div);
      m_comment.SetText(c++,s.Str(),COLOR_TEXT);
   }
   atr = iATR(Symbol(),Period(),14,0);
   if(atr>0)
   {
      (s="ATR {}: {1} pips").Format(PeriodToString(Period()),atr/_Point/div);
      m_comment.SetText(c++,s.Str(),COLOR_TEXT);
   }
   //m_comment.SetText(c++,s.Str(),COLOR_TEXT);
   (s="Risk Target: {0}% of {2} = {2}").Format(m_risk*100,AccountEquity()+m_backup,(AccountEquity()+m_backup)*m_risk);
   m_comment.SetText(c++,s.Str(),COLOR_TEXT);
   if(!m_button.Pressed())
      m_comment.Show();
   double open=Open(),take=Take(),stop=Stop();
   int total = Total();
   if(total == 0)
      m_label.Description("CLICK THE CHART WHERE YOU WANT TO SET THE TRADE LEVELS");
   else
      m_label.Description("");
   if(total < 2)
      return;
   if((open<take && take > 0 && open <stop) || (open>take && take > 0 && open>stop))
   {
      m_label.Description("INVALID TRADE LEVELS");
      return;
   }
   if(stop < open && open < Bid)
      ModDescription(LINE_OPEN,"BUY LIMIT");
   else if(stop < open && open > Bid)
      ModDescription(LINE_OPEN,"BUY STOP");
   else if(stop > open && open < Bid)
      ModDescription(LINE_OPEN,"SELL STOP");  
   else if(stop > open && open > Bid)
      ModDescription(LINE_OPEN,"SELL LIMIT");  
         
   if(stop>0 && open>0)
   {
      double MAR    = NormalizeDouble((AccountEquity()+m_backup) * m_risk,2);
      double points = MathCeil(MathAbs(open-stop) / _Point);
      double tkpoints= MathCeil(MathAbs(open-take) / _Point);
      
      double position= MAR /(pVal * points/div);
      position = NormalizeDouble(position,2);
      position = position<SymbolInfoDouble(Symbol(),SYMBOL_VOLUME_MIN)?SymbolInfoDouble(Symbol(),SYMBOL_VOLUME_MIN):position;
      (s="Position Size: {2} lots").Format(position);
      m_comment.SetText(c++,s.Str(),COLOR_TEXT);
      (s="Max-Loss: {1} pips = -{2}").Format(points/div,points/div*position*pVal);
      m_comment.SetText(c++,s.Str(),COLOR_TEXT);
      double g1= take > 0 ? tkpoints/div : 0;
      double g2= take > 0 ? tkpoints/div*position*pVal : 0;
      (s="Max-Gain: {1} pips = {2}").Format(g1,g2);
      m_comment.SetText(c++,s.Str(),COLOR_TEXT);
      (s="Risk/Reward = [{1}:1]").Format((tkpoints/div*position*pVal)/(points/div*position*pVal));
      m_comment.SetText(c++,s.Str(),COLOR_TEXT);
      if(!m_button.Pressed())
         m_comment.Show();
   }
}
//+------------------------------------------------------------------+
void RiskCalc::OnChartEvent(int id,long lparam,double dparam,string sparam)
{
   bool res = m_comment.OnChartEvent(id,lparam,dparam,sparam);
   if(res==EVENT_CHANGE)
      m_comment.Show();
   if(id==CHARTEVENT_OBJECT_CLICK && sparam==m_button_name)
   {
      if(!m_button.Pressed())  
         Show();
      else
         Hide();
   }
   if(id==CHARTEVENT_CLICK)
   {
      if(!m_button.Pressed())  
         MouseClick((int)lparam,(int)dparam);   
      Calc();
   }   
   if(id==CHARTEVENT_OBJECT_DRAG)
   {
      Calc();
   }
   if(id==CHARTEVENT_CHART_CHANGE)
      ButtonMove();
}
//+------------------------------------------------------------------+
bool RiskCalc::MouseClick(int x, int y)
{
   if(y >= m_zone.y1 && y <= m_zone.y2 && x >= m_zone.x1 && x <= m_zone.x2)
      return false;
   if(m_comment.Zone(x,y))
      return false;
      
   m_clk_cnt++;
   EventSetMillisecondTimer(300);
   if(m_clk_cnt >=3)
   {
      Clear();
      return false;
   }
 
   ClearMissing();
   int total = Total();
   if(total >=3)
      return false;
   double   price=0;
   datetime time=0;
   int      window=0;
   if(!ChartXYToTimePrice(0,x,y,window,time,price))
      return false;
   if(!LineExists(LINE_OPEN))
      return Add(new CalcLine(LINE_OPEN,price));
   if(!LineExists(LINE_STOP))
      return Add(new CalcLine(LINE_STOP,price));
   if(!LineExists(LINE_TAKE))
      return Add(new CalcLine(LINE_TAKE,price));
   
   return false; 
}
//+------------------------------------------------------------------+
void RiskCalc::ButtonMove()
{
   int x = (int)::ChartGetInteger(0,CHART_WIDTH_IN_PIXELS);
   int y = (int)::ChartGetInteger(0,CHART_HEIGHT_IN_PIXELS);
   switch(m_anchor)
   {
      case RIGHT_LOWER: 
         m_button.Move(x-27,y-27);  
         m_zone.x1 = x-30; m_zone.x2=x;
         m_zone.y1 = y-30; m_zone.y2=y;
         m_label.Anchor(ANCHOR_RIGHT_LOWER);
         m_label.Corner(CORNER_RIGHT_LOWER);
         m_label.X_Distance(30);
         break;
      case RIGHT_UPPER: 
         m_button.Move(x-27,2);  
         m_zone.x1 = x-30; m_zone.x2=x;
         m_zone.y1 = 0; m_zone.y2=30;  
         m_label.Anchor(ANCHOR_RIGHT_UPPER);
         m_label.Corner(CORNER_RIGHT_UPPER);
         m_label.X_Distance(30);
         break;
      case LEFT_LOWER:  
         m_button.Move(1,y-27);    
         m_zone.x1 = 0; m_zone.x2=30;
         m_zone.y1 = y-30; m_zone.y2=y;  
         m_label.Anchor(ANCHOR_LEFT_LOWER);
         m_label.Corner(CORNER_LEFT_LOWER);
         m_label.X_Distance(30);
         break;
      case LEFT_UPPER:  
         m_button.Move(1,1); 
         m_zone.x1 = 0; m_zone.x2=30;
         m_zone.y1 = 0; m_zone.y2=30;  
         m_label.Anchor(ANCHOR_LEFT_UPPER); 
         m_label.Corner(CORNER_LEFT_UPPER); 
         m_label.X_Distance(30);      
         break;
   }
}
//+------------------------------------------------------------------+
bool RiskCalc::LineExists(LINE_TYPE type)
{
   for(int i=0;i<Total();i++)
      if(type == this[i].LineType())
         return true;
   return false;
}
//+------------------------------------------------------------------+
void RiskCalc::ClearMissing()
{
   for(int i=Total()-1;i>=0;i--)
   {
      bool found = false;
      for(int j=0;j<ObjectsTotal();j++)
      {
         if(this[i].Name() == ObjectName(j))
         {
            found = true;
            break;
         }
      }
      if(!found)
         Delete(i);
   }
}

//+------------------------------------------------------------------+
bool RiskCalc::ModDescription(LINE_TYPE type,string des)
{
   for(int i=0;i<Total();i++)
   {
      if(type == this[i].LineType())
      {
         return this[i].ModDescription(des);
      }
   }
   return false;
}

//+------------------------------------------------------------------+
double RiskCalc::Open(void)
{
   for(int i=0;i<Total();i++)
   {
      if(this[i].LineType() == LINE_OPEN)
         return this[i].Price();
   }
   return -1;
}
//+------------------------------------------------------------------+
double RiskCalc::Take(void)
{
   for(int i=0;i<Total();i++)
   {
      if(this[i].LineType() == LINE_TAKE)
         return this[i].Price();
   }
   return -1;
}
//+------------------------------------------------------------------+
double RiskCalc::Stop(void)
{
   for(int i=0;i<Total();i++)
   {
      if(this[i].LineType() == LINE_STOP)
         return this[i].Price();
   }
   return -1;
}
//+------------------------------------------------------------------+
void  RiskCalc::Show()
{
   m_label.Create(0,m_label_name,0,m_label_xy.x1,m_label_xy.y1);
   m_label.Color(clrLightGray);
   m_label.FontSize(8);
   ButtonMove();
   m_comment.Show();
   for(int i=0;i<Total();i++)
      this[i].Show();
}
//+------------------------------------------------------------------+
void  RiskCalc::Hide()
{
   m_label.Delete();
   m_comment.Hide();
   for(int i=0;i<Total();i++)
      this[i].Hide();
}
//+------------------------------------------------------------------+
void RiskCalc::OnTimer()     
{ 
   m_clk_cnt = 0; 
   EventKillTimer(); 
};
